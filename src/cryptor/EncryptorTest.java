package cryptor;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class EncryptorTest {

    @Test
    public void testInitAlphabet() {
        // String "J, K" --> Map J -> K
        // String "A, E" --> Map A -> E
        List<String> alphabet = Arrays.asList("J, K", "A, E");
        Encryptor encryptor = new Encryptor();
        encryptor.initAlphabet(alphabet);
        Assertions.assertTrue(encryptor.cryptoMap.get("J").equals("K"));
        Assertions.assertTrue(encryptor.cryptoMap.get("A").equals("E"));
    }
}
