package week6;

public class Ex {

    public static void main(String[] args) {
        System.out.println(tripleWords("Test"));
        System.out.println(censorMessage("Auto sõidab maanteel, vasti jõuab kohale!"));
    }

    private static String tripleWords(String inputText) {

        String tripledCharText = "";

        // Tere --> TTTeeerrreee
        for(int i = 0; i < inputText.length(); i++) {
            char c = inputText.charAt(i);

//            // Variant 1
//            for(int j = 0; j < 3; j++) {
//                tripledCharText += c;
//            }

            // Variant 2
            tripledCharText += String.valueOf(c).repeat(3);
        }

        return tripledCharText;
    }

    private static String censorMessage(String inputText) {
        String resultingText = "";

        for(int i = 0; i < inputText.length(); i++) {
            // Variant 1
//            String character = String.valueOf(inputText.charAt(i));
//            if (
//                    character.equals(".") ||
//                    character.equals(",") ||
//                    character.equals("!") ||
//                    character.equals("?") ||
//                    character.equals(" ")
//            ) {
//                resultingText = resultingText + character;
//            } else {
//                resultingText = resultingText + "#";
//            }

            // Variant 2
            String pattern = "[a-zöäõüA-ZÖÄÕÜŽ0-9]";
            String character = String.valueOf(inputText.charAt(i));
            if (character.matches(pattern)) {
                resultingText = resultingText + "#";
            } else {
                resultingText = resultingText + character;
            }
        }

        return resultingText;
    }

}
