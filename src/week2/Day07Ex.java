package week2;

public class Day07Ex {

    public static void main(String[] args) {

        // Ülesanne 1
        Dog indonesianDog = new IndonesianDog();
        indonesianDog.bark();

        Dog japaneseDog = new JapaneseDog();
        japaneseDog.bark();

        IrishDog irishDog = new IrishDog();
        irishDog.bark();

    }
}
