package week2;

public interface Car {
    void drive();
    void speedUp();
    void stop();
    void addDriver(Human driver);
    int getMaxSpeed();
}
