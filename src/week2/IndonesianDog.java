package week2;

public class IndonesianDog extends Dog {

    @Override
    public void bark() {
        System.out.println("guk-guk, gong-gong! (Indonesian style)");
    }
}
