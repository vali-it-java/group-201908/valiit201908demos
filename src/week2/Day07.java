package week2;

import java.util.ArrayList;
import java.util.List;

public class Day07 {

    public static void main(String[] args) {

        System.out.println("Inimeste arv: " + Human.humanCount); // Klass.muutuja

        Human h1 = new Human("Ragnar");
        changeHumanName(h1); // Objektide puhul antakse edasi objekti referents, objektist koopiat ei tehta.
        System.out.println(h1.name);

        System.out.println("Inimeste arv: " + Human.humanCount);

        int myNumber = 222;
        changePrimitiveNumber(myNumber); // myNumber muutuja väärtusest tehakse koopia ja edasi antakse koopia
        System.out.println(myNumber);

        Integer myNumber2 = 222;
        changePrimitiveNumber2(myNumber2);
        System.out.println(myNumber2);

        List<Car> cars = new ArrayList<>();
        Car myFerrary = new FerrariF355();
        myFerrary.addDriver(h1);
        cars.add(myFerrary);

        cars.get(0).drive();
    }

    private static void changeHumanName(Human x) {
        x.name = "Kris"; // objekt.muutuja
    }

    private static void changePrimitiveNumber(int inputNumber) {
        inputNumber = 555;
    }

    private static void changePrimitiveNumber2(Integer x) {
        x = 555;
    }
}
