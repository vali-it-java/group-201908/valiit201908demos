package week2;

public class Trabant implements Car {

    private Human driver;

    @Override
    public void drive() {
        System.out.println("Moving really slooooowly....");
    }

    @Override
    public void speedUp() {
        System.out.println("Impossible");
    }

    @Override
    public void stop() {
        System.out.println("Already standing, almost...");
    }

    @Override
    public void addDriver(Human driver) {
        this.driver = driver;
    }

    @Override
    public int getMaxSpeed() {
        return 90;
    }
}
