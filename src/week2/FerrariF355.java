package week2;

public class FerrariF355 implements Car {

    private Human driver;

    @Override
    public void drive() {
        System.out.println("Wrmmm...");
    }

    @Override
    public void speedUp() {
        System.out.println("Faster, faster, faster...");
    }

    @Override
    public void stop() {
        System.out.println("No movement anymore!");
    }

    @Override
    public void addDriver(Human driver) {
        this.driver = driver;
    }

    @Override
    public int getMaxSpeed() {
        return 320;
    }
}
