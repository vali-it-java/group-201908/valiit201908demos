package week2;

public class Week1HomeEx {

    public static void main(String[] args) {

        // Ülesanne 1
        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 18; col++) {
                if ((row % 2 == 0 && col % 6 < 3) || (row % 2 == 1 && col % 6 >= 3)) {
                    System.out.print("#");
                } else {
                    System.out.print("+");
                }
            }
            System.out.println();
        }
        System.out.println();

        // Ülesanne 2
        for (int row = 0; row < 9; row++) {
            for (int col = 0; col < 18; col++) {
                if ((row % 6 < 3 && col % 2 == 0) || (row % 6 >= 3 && col % 2 == 1)) {
                    System.out.print("#");
                } else {
                    System.out.print("+");
                }
            }
            System.out.println();
        }
        System.out.println();

        // Ülesanne 3
        for (int row = 0; row < 18; row++) {
            for (int col = 0; col < 18; col++) {
                if (row == col) {
                    System.out.print("#");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
        System.out.println();

        // Ülesanne 4
        for (int row = 0; row < 18; row++) {
            for (int col = 0; col < 18; col++) {
                if (row + col == 17) {
                    System.out.print("#");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }
}
