package week2;

import java.util.ArrayList;
import java.util.List;

public class Day06Ex {

    public static void main(String[] args) {
        List<CountryInfo> countries = new ArrayList<>();
        CountryInfo estonia = new CountryInfo(
                "Estonia",
                "Tallinn",
                "Jüri Ratas",
                new String[]{"Estonian", "Russian", "Finnish", "Latvian"}
        );
        countries.add(estonia);

        countries.add(new CountryInfo("Latvia", "Riga", "Arturs Krišjānis Kariņš", new String[]{"Latvian", "Russian", "Lithuanian"}));
        countries.add(new CountryInfo("Sweden", "Stockholm", "Stefan Löfven", new String[]{"Swedish", "Sami"}));

        for (CountryInfo country : countries) {
            System.out.println(country);
//            System.out.println(country.name + ":");
//            for (String language : country.languages) {
//                System.out.println("    " + language);
//            }
        }

        // Ülesanne 1
        List<Athlete> athletes = new ArrayList<>();

        Runner runner1 = new Runner();
        runner1.firstName = "Thomas";
        runner1.lastName = "More";

        Runner runner2 = new Runner();
        runner2.firstName = "Elon";
        runner2.lastName = "Musk";

        Runner runner3 = new Runner();
        runner3.firstName = "Bill";
        runner3.lastName = "Gates";

        Skydiver skydiver1 = new Skydiver();
        skydiver1.firstName = "Donald";
        skydiver1.lastName = "Trump";

        athletes.add(runner1);
        athletes.add(runner2);
        athletes.add(runner3);
        athletes.add(skydiver1);

        athletes.get(1).perform();
        athletes.get(3).perform();
    }
}
