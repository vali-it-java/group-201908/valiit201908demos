package week2;

public class Human {
    public String name; // Objektimuutuja
    public int age; // Objektimuutuja
    public double weight; // Objektimuutuja

    public static int humanCount = 0; // klassimuutuja

    public Human(String name, int age, double weight) {
        this.name = name; // Objektimuutuja
        this.age = age; // Objektimuutuja
        this.weight = weight; // Objektimuutuja
        // Objektimuutujaid võib olla palju - igal objektil oma koopia.

        Human.humanCount++; // Klassimuutuja (ainult üks eksemplar)
    }

    public Human(String name) {
        this.name = name;
        Human.humanCount++;
    }

    public Human() {
    }

    public boolean isYoung() {
        return age < 100;
    }

    public static boolean isThisHumanYoung(Human x) {
        return x.age < 100;
    }
}
