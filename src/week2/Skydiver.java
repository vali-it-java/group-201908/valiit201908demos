package week2;

public class Skydiver extends Athlete {

    @Override
    public void perform() {
        System.out.println("Falling from the sky...");
    }
}
