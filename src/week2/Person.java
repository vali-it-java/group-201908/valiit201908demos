package week2;

public class Person {

    private String personalCode;

    public Person(String personalCode) {
        this.personalCode = personalCode;
    }

    public int getBirthYear() {
        int centuryKey = Integer.parseInt(personalCode.substring(0, 1));
        int centuryYear = Integer.parseInt(personalCode.substring(1, 3));

        int century;
        switch (centuryKey) {
            case 1:
            case 2:
                century = 1800;
                break;
            case 3:
            case 4:
                century = 1900;
                break;
            case 5:
            case 6:
                century = 2000;
                break;
            case 7:
            case 8:
                century = 2100;
                break;
            default:
                century = 0;
        }

        return century + centuryYear;
    }

    public String getBirthMonth() {
        String birthMonthNumber = personalCode.substring(3, 5);
        switch (birthMonthNumber) {
            case "01":
                return "Jaanuar";
            case "02":
                return "Veebruar";
            case "03":
                return "Märts";
            case "04":
                return "Aprill";
            case "05":
                return "Mai";
            case "06":
                return "Juuni";
            case "07":
                return "Juuli";
            case "08":
                return "August";
            case "09":
                return "September";
            case "10":
                return "Oktoober";
            case "11":
                return "November";
            case "12":
                return "Detsember";
            default:
                return "Ei saa tuletada sünnikuud: isikukood on ebakorrektne.";
        }
    }

    public int getBirthDayOfMonth() {
        return Integer.parseInt(personalCode.substring(5, 7));
    }

    public String getGender() {
        char firstDigitChar = personalCode.charAt(0);
        String firstDigitStr = String.valueOf(firstDigitChar);
        int firstDigitValue = Integer.parseInt(firstDigitStr);
        return firstDigitValue % 2 == 1 ? "M" : "F";
    }
}
