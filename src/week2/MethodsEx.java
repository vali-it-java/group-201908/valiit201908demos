package week2;

public class MethodsEx {

    public static void main(String[] args) {
        test(3); // Ei pea alati tagastatavat väärtust kinni püüdma.
        test2(null, "mingi tekst");
        System.out.println(addVat(100.0));

        String gender = deriveGender("496....");
        System.out.println("Sugu: " + gender);

        printHello();
    }

    static boolean test(int someNumber) {
        return false;
    }

    static void test2(String text1, String text2) {

    }

    static double addVat(double initialPrice) {
        return initialPrice * 1.2;
    }

    // Ülesanne 4
    static int[] generateArray(int a, int b, boolean alphabetical) {
        if (alphabetical) {
            return new int[]{a, b};
        } else {
            return new int[]{b, a};
        }
    }

    // Ülesanne 5
    static String deriveGender(String personalCode) {
//        int digit = personalCode.charAt(0) - '0';

        char firstDigitChar = personalCode.charAt(0);
        String firstDigitStr = String.valueOf(firstDigitChar);
        int firstDigitValue = Integer.parseInt(firstDigitStr);

        return firstDigitValue % 2 == 1 ? "M" : "F";

//        if (firstDigitValue % 2 == 1) {
//            return "M";
//        } else {
//            return "F";
//        }
    }

    // Ülesanne 6
    static void printHello() {
        System.out.println("Tere");
    }
}
