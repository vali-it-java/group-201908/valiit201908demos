package week2;

public class Visit {
    private String date;
    private int count;

    public Visit(String date, int count) {
        this.date = date;
        this.count = count;
    }

    public String getDate() {
        return date;
    }

    public int getCount() {
        return count;
    }
}
