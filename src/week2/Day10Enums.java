package week2;

import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.attribute.BasicFileAttributes;

public class Day10Enums {

    private Gender personGender;

    public Gender getPersonGender() {
        return personGender;
    }

    public static void main(String[] args) {

        Gender myGender = Gender.FEMALE;
        Gender myGender2 = Gender.FEMALE;
        Gender[] genders = Gender.values();

        if (myGender == myGender2) {
            System.out.println("On samast soost");
        } else {
            System.out.println("Ei ole samast soost");
        }

        Gender myGender3 = Gender.valueOf("MALE");

    }
}
