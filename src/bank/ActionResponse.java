package bank;

public class ActionResponse {
    private boolean actionSuccessful;
    private Account fromAccount;
    private Account toAccount;
    private String message;

    public ActionResponse(boolean actionSuccessful, Account fromAccount, Account toAccount, String message) {
        this.actionSuccessful = actionSuccessful;
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
        this.message = message;
    }

    public boolean isActionSuccessful() {
        return actionSuccessful;
    }

    public Account getFromAccount() {
        return fromAccount;
    }

    public Account getToAccount() {
        return toAccount;
    }

    public String getMessage() {
        return message;
    }
}
