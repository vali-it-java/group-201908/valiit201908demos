package bank;

import java.io.IOException;
import java.util.Scanner;

public class BankApp {

    public static Scanner inputReader = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        AccountService.loadAccounts(args[0]);

        while (true) {
            System.out.println();
            System.out.println("Sisesta käsklus:");
            String command = inputReader.nextLine();
            String[] commandParts = command.split(" ");
            switch (commandParts[0].toUpperCase()) {
                case "BALANCE":
                    displayAccountDetails(commandParts);
                    break;
                case "TRANSFER":
                    doTransfer(commandParts);
                    break;
                case "EXIT":
                    return;
                default:
                    System.out.println("Tundmatu käsklus.");
            }
        }

    }

    private static void displayAccountDetails(String[] commandParts) {
        if (commandParts.length == 2) {
            // Küsimine kontonumbri järgi.
            // BALANCE 780141394
            Account x = AccountService.searchAccount(commandParts[1]);
            displayAccountDetails(x);
        } else if (commandParts.length == 3) {
            // Küsimine eesnime ja perenime järgi.
            // BALANCE Carmen Michell
            Account y = AccountService.searchAccount(commandParts[1], commandParts[2]);
            displayAccountDetails(y);
        } else {
            System.out.println("Tundmatu käsklus.");
        }
    }

    private static void displayAccountDetails(Account accountToDisplay) {
        System.out.println("----- KONTO -----");
        if (accountToDisplay != null) {
            System.out.println("Name: " + accountToDisplay.getFirstName() + " " + accountToDisplay.getLastName());
            System.out.println("Number: " + accountToDisplay.getAccountNumber());
            System.out.println("Balance: " + accountToDisplay.getBalance());
        } else {
            System.out.println("Kontot ei leitud");
        }
    }

    private static void doTransfer(String[] commandParts) {
        if (commandParts.length == 4) {
            ActionResponse response = AccountService.transfer(commandParts[1], commandParts[2], Double.parseDouble(commandParts[3]));
            if (response.isActionSuccessful()) {
                System.out.println();
                System.out.println(response.getMessage());
                System.out.println();
                System.out.println("Maksja konto detailandmed: ");
                System.out.println();
                displayAccountDetails(response.getFromAccount());
                System.out.println("Saaja konto detailandmed: ");
                displayAccountDetails(response.getToAccount());
            } else {
                System.out.println();
                System.out.println("*********VIGA! " + response.getMessage());
                if (response.getFromAccount() != null) {
                    System.out.println();
                    System.out.println("Maksja konto detailandmed: ");
                    displayAccountDetails(response.getFromAccount());
                }
            }
        } else {
            System.out.println("Tundmatu käsklus.");
        }
    }
}
