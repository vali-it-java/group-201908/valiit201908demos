package week4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class App {
    public static void main(String[] args) {
        String[] words = {"auto", "rebane", "sau"};
        String[] censoredWords = x(words);

        // Write function with following properties:
        //* input parameters: String, byte
        // * returns type String
        // Functionality: concatenates string and byte as text

//        App myApp = new App();
//        myApp.x2();

        byte b = 5; // täiesti mõttetu muutuja. Sellega ei tehtam midagi.
        System.out.println(x2("A", b));

        /*
            Tekita meetod, mis võtab sisendiks stringide massiivi ja mis tagastab täisarvude massiivi.
            Iga tagastatava massiivi element on vastava sisendmassiivi elemendi pikkus.
            Näide:
            sisend: {"auto", "ratas"}
            väljund: {4, 5}
        */

        x3(new String[] {"word1", "word2"});

        int[] x = x4(new String[]{"a", "aa"});
        System.out.println(x);

        for (int l : x) {
            System.out.println(l);
        }

        List<String> hashesList = x7(9);
        System.out.println(hashesList);

    }

//    public static String[] textsUnifierArray(String a, String b, String c) {
//
//    }

    public static List<String> textsUnifier(String a, String b, String c) {
//        List<String> result = new ArrayList<>();
//        result.add(a);
//        result.add(b);
//        result.add(c);

        List<String> result = Arrays.asList(a, b, c);

        return result;
    }

    public static List<String> x7(int number) {
        List<String> result = new ArrayList<>(); // Pikkust kaasa ei anna.

        for(int i = 0; i < number; i++) {
            result.add(generateHashes(i)); // Lihtsalt lisame lõppu uue elemendi.
        }

        return result;
    }

    public static String[] x6(int num) {
        String[] result = new String[num];
        for(int i = 0; i < num; i++) {
            result[i] = generateHashes(i);
        }
        return result;
    }

    public static String generateHashes(int index) {
        String result = "";
        for(int i = 0; i <= index; i++) {
            result = result + "#";
        }
        return result;
    }

    public static int[] x4(String[] aaa){
        int[] bbb = new int[aaa.length]; // Massiiv on fikseeritud pikkusega

        for (int i = 0; i < aaa.length; i++) {
            bbb[i] = aaa[i].length();
        }

        return bbb;
    }



    public static void x3(String[] a) {

    }

    public static String x2(String s, byte b) {
        return s + b;
    }

    public static String[] x(String[] input) {
        String[] result = new String[input.length];

        for (int i = 0; i < input.length; i++) {
            String tmp = "#".repeat(input[i].length());
            result[i] = tmp;
        }

        return result;
    }
}
