package week4;

public class BodyMassIndex {

    public static void main(String[] args) {
        double bmi = calculateBmi(75.57, 178);
        System.out.println(
                String.format(
                        "Inimene kaaluga 75 kg ja pikkusega 178 cm on kehamassiindeksiga %s", bmi));

        String bmiText = deriveBmiCategory(bmi);
        System.out.println(
                String.format(
                        "Kehamassiindeks %s tähendab kaalukategooriat %s",
                        bmi, bmiText
                )
        );
    }

    private static String deriveBmiCategory(double bmi) {
        if (bmi < 16) {
            return "Tervisele ohtlik alakaal";
        } else if (bmi < 19) {
            return "Alakaal";
        } else if (bmi < 25) {
            return "Normaalkaal";
        } else if (bmi < 30) {
            return "Ülekaal";
        } else if (bmi < 35) {
            return "Rasvumine";
        } else if (bmi < 40)  {
            return "Tugev rasvumine";
        } else {
            return "Tervisele ohtlik rasvumine";
        }
    }

    private static double calculateBmi(double weight, int height) {
        double heightInM = height / 100.0;
        double bmi = weight / (Math.pow(heightInM, 2));
        bmi = Math.round(bmi * 100) / 100.0;
        return bmi;
    }


}
