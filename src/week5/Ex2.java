package week5;

public class Ex2 {

    public static void main(String[] args) {

        EurCurrencyConverter eurUsdConverter = new EurUsdCurrencyConverter();
        System.out.println("112 USA dollarit on " + eurUsdConverter.toEuros(112.0) + " eurot.");
        System.out.println("112 eurot on " + eurUsdConverter.fromEuros(112.0) + " dollarit.");

        EurCurrencyConverter eurBrlconverter = new EurRealCurrencyConverter();
        System.out.println("112 Brasiilia reaali on " + eurBrlconverter.toEuros(112.0) + " eurot.");
        System.out.println("112 eurot on " + eurBrlconverter.fromEuros(112.0) + " Brasiilia reaali.");

        String newLinedText = getNewLinedText("See on mingi tekst.");
        System.out.println(newLinedText);
    }

    public static String getNewLinedText(String inputText) {
        return inputText.replaceAll(" ", "\n");
    }

    public static class EurRealCurrencyConverter extends EurCurrencyConverter {

        @Override
        public double getRate() {
            return 4.4402;
        }
    }

    public static class EurUsdCurrencyConverter extends EurCurrencyConverter {

        @Override
        public double getRate() {
            return 1.0963;
        }
    }

    public static abstract class EurCurrencyConverter {

        public abstract double getRate();

        public double toEuros(double priceInForeignCurrency) {
            return Math.round(priceInForeignCurrency / this.getRate() * 100) / 100.0;
        }

        public double fromEuros(double priceInEuros) {
            return Math.round(priceInEuros * this.getRate() * 100) / 100.0;
        }
    }

}
