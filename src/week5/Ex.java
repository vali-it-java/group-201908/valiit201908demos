package week5;

public class Ex {

    public static void main(String[] args) {
        System.out.println("Miljard sekundit on: " + new Period(1_000_000_000));
        System.out.println("86400 sekundit on: " + new Period(86400));
        System.out.println("84600 sekundit on: " + new Period(84600));
    }

    public static class Period {
        private int years;
        private int months;
        private int days;
        private int hours;
        private int minutes;
        private int seconds;

        public Period(long totalSeconds) {
            this.years = (int)(totalSeconds / (60*60*24*30*12));
            int reminder = (int)(totalSeconds % (60*60*24*30*12));
            this.months = reminder / (60*60*24*30);
            reminder = reminder % (60*60*24*30);
            this.days = reminder / (60*60*24);
            reminder = reminder % (60*60*24);
            this.hours = reminder / (60*60);
            reminder = reminder / (60*60);
            this.minutes = reminder / 60;
            this.seconds = reminder % 60;
        }

        public int getYears() {
            return years;
        }

        public int getMonths() {
            return months;
        }

        public int getDays() {
            return days;
        }

        public int getHours() {
            return hours;
        }

        public int getMinutes() {
            return minutes;
        }

        public int getSeconds() {
            return seconds;
        }

        @Override
        public String toString() {
            return String.format("PERIOD[%s years, %s months, %s days, %s hours, %s minutes, %s seconds]",
                    years, months, days, hours, minutes, seconds);
        }
    }
}
