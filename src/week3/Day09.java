package week3;

public class Day09 {
    public static void main(String[] args) {
        int i = readNumberFromString("asdf");
        System.out.println(i);
    }

    private static int readNumberFromString(String numberText){
        try {
            return Integer.parseInt(numberText);
        } catch (NumberFormatException e) {
            return 0;
        }
    }
}
