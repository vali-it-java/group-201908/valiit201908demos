package week3;

import java.util.Comparator;

public class CarSortingInstructions implements Comparator<Car> {
    @Override
    public int compare(Car o1, Car o2) {
//        if (o1.getMaxSpeed() == o2.getMaxSpeed()) {
//            if (o1.getWeight() == o2.getWeight()) {
//                return o1.getProducer().compareTo(o2.getProducer());
//            } else {
//                return o1.getWeight() - o2.getWeight();
//            }
//        } else {
//            return o1.getMaxSpeed() - o2.getMaxSpeed();
//        }

        if (o1.getMaxSpeed() > o2.getMaxSpeed()) {
            return -1;
        } else {
              if (o1.getMaxSpeed() < o2.getMaxSpeed()) {
                  return 1;
              } else {
                 if (o1.getWeight() > o2.getWeight()) {
                     return -1;
                 } else {
                     return 0;
                 }
              }
        }
    }
}
