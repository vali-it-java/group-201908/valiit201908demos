package week3;

import java.util.Objects;

public class Car {
    private int weight;
    private int maxSpeed;
    private String producer;
    private String model;
    private String dateOfManufacture;

    public Car(int weight, int maxSpeed, String producer, String model, String dateOfManufacture) {
        this.weight = weight;
        this.maxSpeed = maxSpeed;
        this.producer = producer;
        this.model = model;
        this.dateOfManufacture = dateOfManufacture;
    }

    public int getWeight() {
        return weight;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public String getProducer() {
        return producer;
    }

    public String getModel() {
        return model;
    }

    public String getDateOfManufacture() {
        return dateOfManufacture;
    }

    @Override
    public int hashCode() {
        int hash = 11;
        hash = hash * 11 + Objects.hashCode(weight);
        hash = hash * 11 + Objects.hashCode(maxSpeed);
        hash = hash * 11 + Objects.hashCode(producer);
        hash = hash * 11 + Objects.hashCode(model);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true; // Võrdleme objekti iseensaga
        }
        if (obj == null) {
            return false; // Võrdleme objekti mitte millegagi
        }
        if (getClass() != obj.getClass()) {
            return false; // Võrdleme kahe objekti tüüpe. Kui need on erinevad, on objektid erinevad.
        }
        Car otherCar = (Car)obj;
        return this.getWeight() == otherCar.getWeight() &&
                this.getMaxSpeed() == otherCar.getMaxSpeed() &&
                this.getProducer().equals(otherCar.getProducer()) &&
                this.getModel().equals(otherCar.getModel()) &&
                this.getDateOfManufacture().equals(otherCar.getDateOfManufacture());
    }

    @Override
    public String toString() {
        return String.format("[CAR: %d kg, %d km/h, %s, %s, %s]",
                this.getWeight(), this.getMaxSpeed(), this.getProducer(),
                this.getModel(), this.getDateOfManufacture());
    }
}
