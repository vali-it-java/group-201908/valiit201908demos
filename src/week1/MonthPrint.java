package week1;

public class MonthPrint {
    public static void main(String[] args) {
        // Lugege käsurea parameetrina sisse number vahemikus 1 .. 12
        // ja printige ekraanile sellele numbrile vastav kuu.
        // näiteks "2" --> veebruar, "7" --> juuli

        int monthValue = Integer.parseInt(args[0]);
        String monthText = "";

        switch(monthValue) {
            case 1:
                monthText = "jaanuar";
                break;
            case 2:
                monthText = "veebruar";
                break;
            case 3:
                monthText = "märts";
                break;
            case 4:
                monthText = "aprill";
                break;
            case 5:
                monthText = "mai";
                break;
            case 6:
                monthText = "juuni";
                break;
            case 7:
                monthText = "juuli";
                break;
            case 8:
                monthText = "august";
                break;
            case 9:
                monthText = "september";
                break;
            case 10:
                monthText = "oktoober";
                break;
            case 11:
                monthText = "november";
                break;
            case 12:
                monthText = "detsember";
                break;
            default:
                monthText = "VIGA: Sellist kuud pole olemas!";
        }

        System.out.println(monthText);


    }
}
