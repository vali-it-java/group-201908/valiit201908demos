package week1;

import java.util.*;

public class Pranglija {

    private static final int GUESS_COUNT = 3;
    private static Scanner inputReader = new Scanner(System.in);
    private static List<String[]> log = new ArrayList<>();

    public static void main(String[] args) {
        while (true) {
            System.out.println("Mäng algab...");
            System.out.println("Sisesta oma nimi:");
            String playerName = inputReader.nextLine();

            System.out.println("Sisesta genereeritavate arvude miinimumväärtus, mis suurem või võrdne 0:");
            int minValue = getNumericValueFromUser(0, 100_000);

            System.out.println("Sisesta genereeritavate arvude maksimumväärtus:");
            int maxValue = getNumericValueFromUser(minValue + 1, 100_000);

            System.out.println();
            long startTime = System.currentTimeMillis();

            if (performCalculations(minValue, maxValue)) {
                long endTime = System.currentTimeMillis();
                long elapsedSeconds = (endTime - startTime) / 1000;
                System.out.println("Mäng läbi, sul kulus " + elapsedSeconds + " sekundit!");

                // Salvestame tulemuse logisse...
                addLogRecord(playerName, elapsedSeconds, minValue, maxValue);

                // Kuvame tulemused...
                displayResults();
            } else {
                System.out.println("Mäng läbi, sa kaotasid!");
            }
            System.out.println("Mäng on lõppenud. Mida soovid edasi teha?");
            System.out.println("0 - välju mängust");
            System.out.println("1 - alusta uut mängu");
            int playerSelection = getNumericValueFromUser(0, 1);
            if (playerSelection == 0) {
                break;
            }
            // Hack: muidu jääb viimane \n sisse lugemata ja nime sisestamine uue mängu alduses feilib.
            inputReader.nextLine();
        }
    }

    private static boolean performCalculations(int minValue, int maxValue) {
        for (int i = 0; i < GUESS_COUNT; i++) {
            int number1 = generateRandomNumber(minValue, maxValue);
            int number2 = generateRandomNumber(minValue, maxValue);
            int sum = number1 + number2;
            System.out.println(number1 + " + " + number2 + " = ");
            int guessedSum = inputReader.nextInt();
            if (guessedSum != sum) {
                return false;
            }
        }
        return true;
    }

    private static int generateRandomNumber(int minValue, int maxValue) {
        int delta = maxValue - minValue;
        return (int) (Math.random() * (delta + 1)) + minValue;
    }

    private static int getNumericValueFromUser(int minValue, int maxValue) {
        int userValue = 0;
        while (true) {
            try {
                userValue = inputReader.nextInt();
                if (userValue >= minValue && userValue <= maxValue) {
                    return userValue;
                } else {
                    System.out.println(String.format("Ebakorrektne väärtus, number peab olema vahemikus %d ja %d", minValue, maxValue));
                }
            }
            catch (InputMismatchException e) {
                System.out.println("VIGA: Sisestatud väärtus ei ole täisarv!");
                inputReader.nextLine();
            }
        }
    }

    private static void addLogRecord(String playerName, long elapsedSeconds, int rangeLowerBoundary, int rangeUpperBoundary) {
        String[] result = {playerName, String.valueOf(elapsedSeconds), rangeLowerBoundary + ".." + rangeUpperBoundary};
        log.add(result);

        // Sorteerime tulemused aja järgi kasvavalt.
        Collections.sort(log, (result1, result2) -> {
            int result1Time = Integer.parseInt(result1[1]); // 10
            int result2Time = Integer.parseInt(result2[1]); // 11
            return result1Time - result2Time;
        });
    }

    private static void displayResults() {
        System.out.println("--------------------------------");
        System.out.println("TULEMUSED");
        System.out.println("--------------------------------");
        for (String[] resultItem : log) {
            System.out.println("Nimi: " + resultItem[0]);
            System.out.println("Aeg sekundites: " + resultItem[1]);
            System.out.println("Vahemik: " + resultItem[2]);
            System.out.println("--------------------------------");
        }
    }
}
