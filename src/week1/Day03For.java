package week1;

import java.util.Scanner;

public class Day03For {

    public static void main(String[] args) {

        for (int i = 0; i <= 100; i++) {
            System.out.println(i);
        }

        for (int i = 100; i > 0; i -= 5) {
            System.out.println(i);
        }

        boolean notEnoughMoney = true;
        int sum = 0;
//        while (notEnoughMoney) {
//            sum = sum + 50; // kutsume välja mingi funktsiooni, mis tagastab meile lisanduva rahahulga
//            if (sum >= 5000) {
//                notEnoughMoney = false;
//            }
//        }
        while (true) {
            sum = sum + 50; // kutsume välja mingi funktsiooni, mis tagastab meile lisanduva rahahulga
            if (sum >= 5000) {
                break;
            }
        }
        System.out.println("Potentsiaalne teenitav raha: " + sum);

        Scanner scanner = new Scanner(System.in);
        int insertedValue = 0;
        int checkNumber = 5;

//        do {
//            System.out.println("Sisesta palun maagiline number:\n");
//            String inputText = scanner.nextLine();
//            inputText = inputText.trim();
//            insertedValue = Integer.parseInt(inputText);
//        } while (checkNumber != insertedValue); // Tõene siis, kui ükse ei võrdu teisega
//
//        System.out.println("Tubli, arvasid ära!");

        // Foreach tsükkel
        String[] texts = {"Tallinn", "Tartu", "Narva"};
        for(String temporaryTextValue : texts) {
            System.out.println(temporaryTextValue + " on Eesti linn.");
        }
    }
}
