package week1;

import java.util.*;

public class Day04 {

    public static void main(String[] args) {

        // ------------------
        // List
        // ------------------
        List<String> names = new ArrayList<>();
        names.add("Thomas");
        names.add("Mary");
        names.add("Linda");
        System.out.println(names);

        // Mis kohal asub Linda? Kui lindat pole listis, tagastatakse -1
        int inertPosition = names.indexOf("Linda");
        System.out.println(inertPosition);

        // Lisa etteantud indeksi kohale uus väärtus, järgmised väärtused nihkuvad ühe võrra edasi.
        names.add(2, "Ruth");
        names.add(inertPosition, "Ruth");
        System.out.println(names);

        // Kas antud väärtus on juba listis olemas?
        System.out.println(names.contains("Linda"));
        System.out.println(names.contains("Joe"));

        // Asenda element indeksi kohal X...
        int lindaPosition = names.indexOf("Linda");
        if (lindaPosition >= 0) {
            names.set(lindaPosition, "Laura");
        }
        System.out.println(names);

        for(String name : names) {
            System.out.println("Nimi: " + name);
        }
        System.out.println(names.get(3)); // Kindla indeksiga elemendi pärimine.

        // Mitmetasandiline List
        List<List<String>> complexList = new ArrayList<>();

        List<String> humans = Arrays.asList("Mark", "Mary", "Thomas", "Linda");
        complexList.add(humans);

        List<String> cars = Arrays.asList("BMW", "Renault", "Open", "Ford");
        complexList.add(cars);

        List<String> planes = Arrays.asList("Tupolev", "Jakovlev", "Boeing", "Airbus");
        complexList.add(planes);

        complexList.add(Arrays.asList("Merkuur", "Veenus", "Maa"));

        System.out.println(complexList);
        System.out.println(complexList.get(2).get(1));

        // ------------------
        // Set
        // ------------------
        Set<String> uniqueNames = new HashSet<>();
        uniqueNames.add("Mary");
        uniqueNames.add("Thomas");
        uniqueNames.add("Mark");
        uniqueNames.add("Mary");

        System.out.println(uniqueNames);
        Object[] uniqueNamesArr = uniqueNames.toArray();

        for (String uniqueName : uniqueNames) {
            System.out.println("Unikaalne nimi: " + uniqueName);
        }

        uniqueNames.remove("Mark");
        System.out.println(uniqueNames);

        //uniqueNames.size()

        Comparator<String> nameComparator = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o2.compareTo(o1);
            }
        };
        Comparator<String> nameComparator2 = (n1, n2) -> n2.compareTo(n1);

//        Set<String> sortedNames = new TreeSet<>(nameComparator);
        Set<String> sortedNames = new TreeSet<>(nameComparator2);
        sortedNames.add("Mary");
        sortedNames.add("Thomas");
        sortedNames.add("Mark");
        System.out.println(sortedNames);

        // ------------------
        // Map
        // ------------------
        // Võtme ja väärtuse paaride kogum.
        // Võti1 --> Väärtus1
        // Võti2 --> Väärtus2
        // TreeX (TreeSet, TreeMap) - sorteerimise üle saad ise otsustada.
        // HashX (HashSet, HashMap) - Java virtuaalmasin otsustab sinu eest järjekorra.
        Map<String, String> phoneNumbers = new TreeMap<>(nameComparator2);
        phoneNumbers.put("Mati", "+37255 786 234");
        phoneNumbers.put("Kati", "+37250 166 335");
        phoneNumbers.put("Mari", "+37250 166 335");

        for (String key : phoneNumbers.keySet()) {
            System.out.println("Sõber: " + key + ", telefon: " + phoneNumbers.get(key));
        }

        phoneNumbers.put("Mari", "004867890965");
        phoneNumbers.put("Leonardo", "003858734565644");
        System.out.println(phoneNumbers);

        Map<String, Map<String, String>> contacts = new TreeMap<>();

        Map<String, String> mariContactDetails = new TreeMap<>();
        mariContactDetails.put("Phone", "346756784568");
        mariContactDetails.put("Email", "mari@kari.ee");
        mariContactDetails.put("Address", "Laeva 9-15, Elva");
        contacts.put("Mari", mariContactDetails);

        Map<String, String> martContactDetails = new TreeMap<>();
        martContactDetails.put("Phone", "2354231234");
        martContactDetails.put("Email", "mart@mail.ee");
        martContactDetails.put("Address", "Puu 45-15, Võru");
        contacts.put("Mart", martContactDetails);

        System.out.println(contacts);
        System.out.println("Mardi telefoninumber: " + contacts.get("Mart").get("Phone"));
    }
}
