package week1;

import java.util.*;

public class Day04Ex {

    public static void main(String[] args) {

        // Ülesanne 19
        List<String> cities = new ArrayList<>();
        cities.add("Tartu");
        cities.add("Tallinn");
        cities.add("Võru");
        cities.add("Narva");
        cities.add("Paldiski");
        System.out.println("Esimene element: " + cities.get(0));
        System.out.println("Kolmas element: " + cities.get(3));
        System.out.println("Viimane element: " + cities.get(cities.size() - 1));

        // Ülesanne 20
        // First In, First Out (FIFO)
        Queue<String> names = new LinkedList<>();
        names.add("Mary");
        names.add("Henry");
        names.add("Mark");
        names.add("Manfred");
        names.add("Ursula");
        names.add("Angela");
        while (!names.isEmpty()) {
            System.out.println("Tõmbasin queuest sellise nime: " + names.remove());
        }

        // Ülesanne 21
        Set<String> nameSet = new TreeSet<>();
        nameSet.add("Mary");
        nameSet.add("Henry");
        nameSet.add("Mark");
        nameSet.add("Manfred");
        nameSet.add("Ursula");
        nameSet.add("Angela");
        nameSet.forEach(myNameToPrint -> System.out.println("Lambda-avaldis prindib: " + myNameToPrint));
//        nameSet.forEach(System.out::println);

        // Ülesanne 22
        System.out.println();
        Map<String, String[]> countryCities = new HashMap<>();
        countryCities.put("Estonia", new String[]{"Tallinn", "Tartu", "Valga", "Võru"});
        countryCities.put("Sweden", new String[]{"Stockholm", "Uppsala", "Lund", "Köping"});
        countryCities.put("Finland", new String[]{"Helsinki", "Espoo", "Hanko", "Jämsä"});

        for(String country : countryCities.keySet()) {
            System.out.println("Country: " + country);
            String[] currentCountryCities = countryCities.get(country);
            System.out.println("Cities: ");
            for (String city : currentCountryCities) {
                System.out.println("\t" + city);
            }
        }
    }
}
