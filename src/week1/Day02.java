package week1;

public class Day02 {

    public static void main(String[] args) {
        byte b1 = 1; // Suuris: 1 bait. Väike number
        char c1 = 'B'; // Suurus: 1 bait. Üks tähemärk (NB! Üksikud jutumärgid)
        short s1 = 4578; // Suurus: 2 baiti. Keskmise suurusega number.
        int i1 = 56_569_098; // Suurus: 4 baiti. Suured numbrid.
        long l1 = 10_000_000_000_000L; // Suurus: 8 baiti. Väga suured numbrid.
        boolean isItSpring = false; // Suurus: 1 bait. Tõeväärtus - tõene, väär.
        float f1 = 45.678F; // Suurus: 4 baiti. Komakohaga arv (võrdlemisi suur, aga ebatäpne)
        double d1 = 567.98765; // Suurus: 8 baiti. Komakohaga arv (veelgi suurem, aga ikkagi ebatäpne)

//        System.out.println((char)b1); // casting: byte --> char
//        System.out.println(c1);
        long l2 = i1;
        i1 = (int) l2;

        // Suurema numbri väiksemasse tüüpi surumine jõuga: andmekadu.
        long l3 = 10_000_000_000_000L;
        int i3 = (int) l3;
//        System.out.println(i3);

        // Operaatorid

        // ==
        int i4 = 5;
        int i5 = 2;
        int i6 = 5;
        boolean test1 = i4 == i5;
//        System.out.println(test1);
//        System.out.println(i4 == i6);

        // ++
        int i7 = 50;
        System.out.println(i7++);
        System.out.println(i7);

        int i8 = 60;
        System.out.println(++i8);

        // +=
        int i9 = 11;
        i9 = i9 + 5; // Variant 1
        i9 += 5; // Variant 2

        // Loogiline või ||
        // Üks või teine või mõlemad;
        boolean b10 = false;
        boolean b11 = false;
        System.out.println(b10 || b11);

        // Loogiline and &&
        boolean b12 = false;
        boolean b13 = true;
        System.out.println(b12 && b13);

        // Mooduliga jagamine
        int i14 = 11;
        int i15 = 10;
        System.out.println(i14 % 10);
        System.out.println(i15 % 10);

        // Wrapper klassid
        Integer i20 = 20;
        Integer i21 = new Integer(20);

        //        Loo kolm arvulist muutujat a = 1, b = 1, c = 3
        int a = 1;
        int b = 1;
        int c = 3;
        //        Prindi välja a == b ja a == c
        System.out.println("a==b tulemus: " + (a == b) + ", a==c tulemus: " + (a == c));
//        System.out.println(a == c);
        //        Lisa koodi rida a = c
        a = c; // Muutuja a saab uue väärtuse.
        //        Prindi välja a == b ja a == c, mis muutus ja miks?
        System.out.println(a == b);
        System.out.println(a == c);

        //Loo muutujad x1 = 10 ja x2 = 20, vali sobiv andmetüüp
        int x1 = 10;
        int x2 = 20;
        //Tekita muutuja y1 = ++x1, trüki välja nii x1 kui y1
        int y1 = ++x1;
        System.out.println("x1: " + x1);
        System.out.println("y1: " + y1);
        //Tekita muutuja y2 = x2++, trüki välja nii x2 ja y2
        int y2 = x2++;
        System.out.println(x2);
        System.out.println(y2);

        // Stringitöötlus

        System.out.println("Isa ütles: \"Tule siia\"");
        System.out.println("Minu faili asukoht: C:\\test\\test.txt");
        System.out.println("See on esimene rida.\n See on teine rida.");

        String myText1 = "This is a text.";
        String myText2 = "This is another text.";
        myText1 = myText1 + myText2;

        String myText3 = "T3";
        String myText4 = "T3";
        System.out.println(myText3 == myText4); // Ebakorrektne viis stringide võrdlemiseks (siin sa võrdled tegelikult mälupiirkonna aadresse)!!!
        System.out.println(myText3.equals(myText4)); // Korrektne viis stringide võrdlemiseks.
        System.out.println(myText3.equalsIgnoreCase("t3")); // Korrektne viis stringide võrdlemiseks.
        String myText5 = new String("Some text...");

        // String Processing (harjutused)
        System.out.println("Hello, World!");
        System.out.println();
        System.out.println("Hello, \"World\"!");
        System.out.println();
        System.out.println("Steven hawking once said: \"Life would be tragic if it weren't funny\".");
        System.out.println();
        System.out.println("Kui liita kokku sõned \"See on teksti esimene pool \" ning \"See\n" +
                "on teksti teine pool\", siis tulemuseks saame \"See on teksti\n" +
                "esimene pool See on teksti teine pool\".");
        System.out.println();
        System.out.println("Elu on ilus.\n");
        System.out.println("Elu on 'ilus'.\n");
        System.out.println("Elu on \"ilus\"\n");
        System.out.println("Kõige rohkem segadust tekitab \"-märgi kasutamine sõne sees.\n");
        System.out.println("Eesti keele kõige ilusam lause on: \"Sõida tasa üle silla!\"\n");
        System.out.println("'Kolm' - kolm, 'neli' - neli, \"viis\" - viis.\n");

        // Ülesanne 1
        String tallinnPopulation = "450 000";
        System.out.println("Tallinnas elab " + tallinnPopulation + " inimest.");
        int populationOfTallinn = 450_000;
        System.out.println("Tallinnas elab " + populationOfTallinn + " inimest.");

        // Ülesanne 2
        String bookTitle = "Rehepapp";
        System.out.println(String.format("Raamatu \"%s\" autor on Andrus Kivirähk.", bookTitle));

        // Ülesanne 3
        String planet1 = "Merkuur";
        String planet2 = "Veenus";
        String planet3 = "Maa";
        String planet4 = "Marss";
        String planet5 = "Jupiter";
        String planet6 = "Saturn";
        String planet7 = "Uraan";
        String planet8 = "Neptuun";
        int planetCount = 8;
        System.out.println(
                planet1 + ", " + planet2 + ", " + planet3 + ", " + planet4 + ", " +
                        planet5 + ", " + planet6 + ", " + planet7 + " ja " + planet8 +
                        " on Päikesesüsteemi " + planetCount + " planeeti.");
        System.out.println(
                String.format(
                    "%s, %s, %s, %s, %s, %s, %s ja %s on Päikesesüsteemi %s planeeti.",
                    planet1, planet2, planet3, planet4, planet5, planet6, planet7, planet8, planetCount
                    )
        );

        int populationOfTallinn2 = 450_000;
        System.out.println(String.format("Tallinnas elab %,d inimest.", populationOfTallinn2));
    }

}
