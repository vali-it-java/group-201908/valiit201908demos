package week1;

import java.util.*;

public class Day05Ex {

    public static void main(String[] args) {

        // Ülesanne 1
        System.out.println("----------------------");
        System.out.println("Ülesanne 1");
        System.out.println("----------------------");
        int sideLength = 6;
        for (int row = 0; row < sideLength; row++) {
            int columnCount = sideLength - row; // Iga read jaoks tuleb arvutada oma veergude arv.
            // ROWCOUNT + COLCOUNT = 6
            // COLCOUNT = 6 - ROWCOUNT
            // row 0 -> 6 col (veerud 0..5)
            // row 1 -> 5 col (veerud 0..4)
            // row 2 -> 4 col (veerud 0..3)
            // 0 --> 6
            // 1 --> 5
            // 2 --> 4
            // ...
            for (int column = 0; column < columnCount; column++) {
                System.out.print("#");
            }
            System.out.println();
        }

        // Ülesanne 2
        System.out.println("----------------------");
        System.out.println("Ülesanne 2");
        System.out.println("----------------------");
        // Variant 1
        for (int row = 0; row < sideLength; row++) {
            for (int col = 0; col <= row; col++) {
                System.out.print("#");
            }
            System.out.println();
        }
        // Variant 2
        System.out.println();
        for (int row = 0; row < sideLength; row++) {
            for (int col = 0; col < sideLength; col++) {
                if (col <= row) {
                    System.out.print("#");
                }
            }
            System.out.println();
        }

        // Ülesanne 3
        System.out.println("----------------------");
        System.out.println("Ülesanne 3");
        System.out.println("----------------------");
        int sideLength2 = 5;
        for (int row = 0; row < sideLength2; row++) {
            for (int col = 0; col < sideLength2; col++) {
                if (col < sideLength2 - row - 1) {
                    System.out.print(" ");
                } else {
                    System.out.print("@");
                }
            }
            System.out.println();
        }

        // Ülesanne 4
        System.out.println("----------------------");
        System.out.println("Ülesanne 4");
        System.out.println("----------------------");
        // Variant 1
        // Vihje:
//        String test = "test";
//        char[] testCharArray = test.toCharArray();
        int number = 1234567;
        String numberText = String.valueOf(number); // "1234567"
        char[] numberChars = numberText.toCharArray(); // "1234567" -> { '1', '2', '3', '4', '5', '6', '7' }
        String reversedNumberText = "";
        for (int i = 0; i < numberChars.length; i++) {
            // Lahendus siin...
            reversedNumberText = numberChars[i] + reversedNumberText;
        }

//        for (char c : numberChars) {
//            reversedNumberText = c + reversedNumberText;
//        }

        // Siin tee numbri stringist täisarv.
        int reversedNumber = Integer.parseInt(reversedNumberText);
        // Siin pringi see välja.
        System.out.println(reversedNumber);

        // Variant 2
        System.out.println();
        int number2 = 1234567;
        String numberText2 = String.valueOf(number2);
        StringBuilder numberReverseBuilder = new StringBuilder(numberText2);
        String reversedNumberText2 = numberReverseBuilder.reverse().toString();
        int reversedNumber2 = Integer.parseInt(reversedNumberText2);
        System.out.println(reversedNumber2);

        System.out.println();
        System.out.println(new StringBuilder("1234567").reverse().toString());

        // Ülesanne 5
        System.out.println("----------------------");
        System.out.println("Ülesanne 5");
        System.out.println("----------------------");

//        String studentInfo = "Marek 99";
//        String[] studentInfoArray = studentInfo.split(" "); // {"Marek", "99"}
        if (args.length >= 2) { // Kas on üldse parameetreid, mida analüüsida?
            String studentName = args[0]; // args - main() meetodi sisendparameeter!
            int studentScore = Integer.parseInt(args[1]); // "99" (tekst) --> 99 (number)
            String resultingText = studentName + ": ";

            // Siin tuleb resultingText valmis teha...
            if (studentScore > 90) {
                resultingText = resultingText + "PASS - 5, " + studentScore;
            } else if (studentScore > 80) {
                resultingText = resultingText + "PASS - 4, " + studentScore;
            } else if (studentScore > 70) {
                resultingText = resultingText + "PASS - 3, " + studentScore;
            } else if (studentScore > 60) {
                resultingText = resultingText + "PASS - 2, " + studentScore;
            } else if (studentScore > 50) {
                resultingText = resultingText + "PASS - 1, " + studentScore;
            } else {
                resultingText = "FAIL";
            }

            System.out.println(resultingText);
        }

        // Ülesanne 6
        System.out.println("----------------------");
        System.out.println("Ülesanne 6");
        System.out.println("----------------------");
        double[][] triangleSideList = {
                {1.5, 7.7},
                {14.31, 56.72},
                {18.51, 21.67},
                {798.45, 983.5},
                {61.92, 7.21}
        };

        for (double[] triangleSides : triangleSideList) {
            double c = Math.sqrt(Math.pow(triangleSides[0], 2) + Math.pow(triangleSides[1], 2));
            System.out.println("Kolmnurga hüpotenuusi pikkus: " + c);
        }

        // Ülesanne 7
        System.out.println("----------------------");
        System.out.println("Ülesanne 7");
        System.out.println("----------------------");
        String[][] countries = {
                {"Estonia", "Tallinn", "Jüri Ratas"},
                {"Latvia", "Riga", "Arturs Krišjānis Kariņš"},
                {"Lithuania", "Vilnius", "Saulius Skvernelis"},
                {"Germany", "Berlin", "Angela Merkel"},
                {"Sweden", "Stockholm", "Stefan Löfven"},
                {"Finland", "Helsinki", "Antti Rinne"},
                {"Russia", "Moscow", "Dmitry Medvedev"},
                {"Norway", "Oslo", "Erna Solberg"},
                {"Denmark", "Copenhagen", "Mette Frederiksen"},
                {"Iceland", "Reykjavik", "Katrín Jakobsdóttir"}
        };

        for (int i = 0; i < countries.length; i++) {
            System.out.println(countries[i][2]);
        }
//            for (String[] country : countries) {
//                System.out.println(country[2]);
//            }

        for (int i = 0; i < countries.length; i++) {
            System.out.println(String.format("Country: %s, Capital: %s, Prime minister: %s", countries[i][0], countries[i][1], countries[i][2]));
        }

        // Ülesanne 8
        System.out.println("----------------------");
        System.out.println("Ülesanne 8");
        System.out.println("----------------------");
        // String on Objekt
        // Stringide massiiv on Objekt
        // String[] arr1 = {"aa", "bb"};
        // Object arr2 = {"aa", "bb"};
        // Object str1 = "tere";

        Object[][] countries2 = {
                {"Estonia", "Tallinn", "Jüri Ratas", new String[]{"Estonian", "Russian"}},
                {"Latvia", "Riga", "Arturs Krišjānis Kariņš", new String[]{"Latvian", "Russian"}},
                {"Lithuania", "Vilnius", "Saulius Skvernelis", new String[]{"Lithuanian", "Polish"}},
                {"Germany", "Berlin", "Angela Merkel", new String[]{"German", "French"}},
                {"Sweden", "Stockholm", "Stefan Löfven", new String[]{"Swedish", "Sami"}},
                {"Finland", "Helsinki", "Antti Rinne", new String[]{"Finnish", "Swedish"}},
                {"Russia", "Moscow", "Dmitry Medvedev", new String[]{"Russian", "English"}},
                {"Norway", "Oslo", "Erna Solberg", new String[]{"Norwegian", "Sami"}},
                {"Denmark", "Copenhagen", "Mette Frederiksen", new String[]{"Danish", "German"}},
                {"Iceland", "Reykjavik", "Katrín Jakobsdóttir", new String[]{"Icelandic", "English"}}
        };

        for (Object[] country : countries2) {
            System.out.println(country[0] + ":");
            for (String language : (String[]) country[3]) {
                System.out.print(language + " ");
            }
            System.out.println();
            System.out.println();
        }

        // Ülesanne 9
        System.out.println("----------------------");
        System.out.println("Ülesanne 9");
        System.out.println("----------------------");

        List<List<Object>> countries3 = Arrays.asList(
                Arrays.asList("Estonia", "Tallinn", "Jüri Ratas", Arrays.asList("Estonian", "Russian")),
                Arrays.asList("Latvia", "Riga", "Arturs Krišjānis Kariņš", Arrays.asList("Latvian", "Russian")),
                Arrays.asList("Lithuania", "Vilnius", "Saulius Skvernelis", Arrays.asList("Lithuanian", "Polish")),
                Arrays.asList("Germany", "Berlin", "Angela Merkel", Arrays.asList("German", "French")),
                Arrays.asList("Sweden", "Stockholm", "Stefan Löfven", Arrays.asList("Swedish", "Sami")),
                Arrays.asList("Finland", "Helsinki", "Antti Rinne", Arrays.asList("Finnish", "Swedish")),
                Arrays.asList("Russia", "Moscow", "Dmitry Medvedev", Arrays.asList("Russian", "English")),
                Arrays.asList("Norway", "Oslo", "Erna Solberg", Arrays.asList("Norwegian", "Sami")),
                Arrays.asList("Denmark", "Copenhagen", "Mette Frederiksen", Arrays.asList("Danish", "German")),
                Arrays.asList("Iceland", "Reykjavik", "Katrín Jakobsdóttir", Arrays.asList("Icelandic", "English"))
        );

        for (List<Object> country : countries3) {
            System.out.println(country.get(0) + ":");
            for (String language : (List<String>) country.get(3)) {
                System.out.print(language + " ");
            }
            System.out.println();
            System.out.println();
        }

        // Ülesanne 10
        System.out.println("----------------------");
        System.out.println("Ülesanne 10");
        System.out.println("----------------------");

        Comparator<String> countryNameLengthComparator = (countryName1, countryName2) -> {
            if (countryName1.length() == countryName2.length()) {
                return countryName1.compareTo(countryName2);
            } else {
                return countryName1.length() - countryName2.length();
            }
        };

        Map<String, List<Object>> countryMap = new TreeMap<>(countryNameLengthComparator);
        countryMap.put("Estonia", Arrays.asList("Tallinn", "Jüri Ratas", Arrays.asList("Estonian", "Russian")));
        countryMap.put("Latvia", Arrays.asList("Riga", "Arturs Krišjānis Kariņš", Arrays.asList("Latvian", "Russian")));
        countryMap.put("Lithuania", Arrays.asList("Vilnius", "Saulius Skvernelis", Arrays.asList("Lithuanian", "Polish")));
        countryMap.put("Germany", Arrays.asList("Berlin", "Angela Merkel", Arrays.asList("German", "French")));
        countryMap.put("Sweden", Arrays.asList("Stockholm", "Stefan Löfven", Arrays.asList("Swedish", "Sami")));
        countryMap.put("Finland", Arrays.asList("Helsinki", "Antti Rinne", Arrays.asList("Finnish", "Swedish")));
        countryMap.put("Russia", Arrays.asList("Moscow", "Dmitry Medvedev", Arrays.asList("Russian", "English")));
        countryMap.put("Norway", Arrays.asList("Oslo", "Erna Solberg", Arrays.asList("Norwegian", "Sami")));
        countryMap.put("Denmark", Arrays.asList("Copenhagen", "Mette Frederiksen", Arrays.asList("Danish", "German")));
        countryMap.put("Iceland", Arrays.asList("Reykjavik", "Katrín Jakobsdóttir", Arrays.asList("Icelandic", "English")));

        for (String countryName : countryMap.keySet()) {
            System.out.println(countryName + ":");
            List<String> languages = (List<String>) countryMap.get(countryName).get(2);

//            languages.forEach(language -> System.out.println(language + " "));

            for (String language : languages) {
                System.out.println(language + " ");
            }
        }

        // Ülesanne 11
        System.out.println("----------------------");
        System.out.println("Ülesanne 11");
        System.out.println("----------------------");

        Queue<List<Object>> countryQueue = new LinkedList<>();
        countryQueue.add(Arrays.asList("Estonia", "Tallinn", "Jüri Ratas", Arrays.asList("Estonian", "Russian")));
        countryQueue.add(Arrays.asList("Latvia", "Riga", "Arturs Krišjānis Kariņš", Arrays.asList("Latvian", "Russian")));
        countryQueue.add(Arrays.asList("Lithuania", "Vilnius", "Saulius Skvernelis", Arrays.asList("Lithuanian", "Polish")));
        countryQueue.add(Arrays.asList("Germany", "Berlin", "Angela Merkel", Arrays.asList("German", "French")));
        countryQueue.add(Arrays.asList("Sweden", "Stockholm", "Stefan Löfven", Arrays.asList("Swedish", "Sami")));
        countryQueue.add(Arrays.asList("Finland", "Helsinki", "Antti Rinne", Arrays.asList("Finnish", "Swedish")));
        countryQueue.add(Arrays.asList("Russia", "Moscow", "Dmitry Medvedev", Arrays.asList("Russian", "English")));
        countryQueue.add(Arrays.asList("Norway", "Oslo", "Erna Solberg", Arrays.asList("Norwegian", "Sami")));
        countryQueue.add(Arrays.asList("Denmark", "Copenhagen", "Mette Frederiksen", Arrays.asList("Danish", "German")));
        countryQueue.add(Arrays.asList("Iceland", "Reykjavik", "Katrín Jakobsdóttir", Arrays.asList("Icelandic", "English")));

        while (!countryQueue.isEmpty()) {
            List<Object> country = countryQueue.remove();
            System.out.println(country.get(0) + ": ");
            for (String language : (List<String>) country.get(3)) {
                System.out.print(language + " ");
            }
            System.out.println();
            System.out.println();
        }

        // Lisaülesanne 1
        System.out.println("----------------------");
        System.out.println("Lisaülesanne 1");
        System.out.println("----------------------");

        int rowsCount = 8;
        int colCount = 19;

        for (int row = 0; row < rowsCount; row++) {
            for (int col = 0; col < colCount; col++) {
                if ((col + row) % 2 == 0) {
                    System.out.print("#");
                } else {
                    System.out.print("+");
                }
            }

//            for (int col = 0; col < colCount; col++) {
//                if (row % 2 == 0) {
//                    if (col % 2 == 0) {
//                        System.out.print("#");
//                    } else {
//                        System.out.print("+");
//                    }
//                } else {
//                    if (col % 2 == 0) {
//                        System.out.print("+");
//                    } else {
//                        System.out.print("#");
//                    }
//                }
//            }
            System.out.println();
        }
    }
}

