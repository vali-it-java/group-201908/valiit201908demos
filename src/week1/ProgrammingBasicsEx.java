package week1;

public class ProgrammingBasicsEx {
    public static void main(String[] args) {

        // Koodikontsruktsioonid

        // Ülesanne 2
        String city = "Berlin";
        if (city.equals("Milano")) {
            System.out.println("Ilm on ilus.");
        } else {
            System.out.println("Ilm polegi kõige tähtsam.");
        }

        // Ülesanne 3
        int grade = 5;
        if (grade == 1) {
            System.out.println("Nõrk");
        } else if (grade == 2) {
            System.out.println("Mitterahuldav");
        } else if (grade == 3) {
            System.out.println("Rahuldav");
        } else if (grade == 4) {
            System.out.println("Hea");
        } else if (grade == 5) {
            System.out.println("Väga hea");
        } else {
            System.out.println("VIGA: Ebakorrektne hinne!");
        }

        // Ülesanne 4
        switch (grade) {
            case 1:
                System.out.println("Nõrk");
                break;
            case 2:
                System.out.println("Mitterahuldav");
                break;
            case 3:
                System.out.println("Rahuldav");
                break;
            case 4:
                System.out.println("Hea");
                break;
            case 5:
                System.out.println("Väga hea");
                break;
            default:
                System.out.println("Viga: Ebakorrektne hinne!");
        }

        // Ülesanne 5
        int age = 100;
        String textToPrint = age <= 100 ? "Noor" : "Vana";
        System.out.println(textToPrint);

        // Ülesanne 6
        textToPrint = (age < 100) ? ("Noor") : ((age == 100) ? ("Peaaegu vana") : ("Vana"));
        System.out.println(textToPrint);

    }
}
