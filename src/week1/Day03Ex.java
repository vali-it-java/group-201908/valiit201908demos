package week1;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Day03Ex {

    public static void main(String[] args) {

        // Ülesanne 7
        int[] myIntArr;
        myIntArr = new int[5];
        myIntArr[0] = 1;
        myIntArr[1] = 2;
        myIntArr[2] = 3;
        myIntArr[3] = 4;
        myIntArr[4] = 5;

        System.out.println(String.format("Massiivi esimese elemendi väärtus: %s", myIntArr[0]));
        System.out.println(String.format("Massiivi kolmanda elemendi väärtus: %s", myIntArr[2]));
        System.out.println(String.format("Massiivi viimase elemendi väärtus: %s", myIntArr[myIntArr.length - 1]));

        // Ülesanne 8
        String[] cities = {"Tallinn", "Helsinki", "Madrid", "Paris"};

        // Ülesanne 9
        // Variant 1 (lihtsam)
        int[][] myTwoDimensionalArray = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9, 0}
        };

        // Variant 2 (pikem)
        int[][] myTwoDimensionalArray2 = new int[3][3];
        myTwoDimensionalArray2[0] = new int[3];
        myTwoDimensionalArray2[0][0] = 1;
        myTwoDimensionalArray2[0][1] = 2;
        myTwoDimensionalArray2[0][2] = 3;
        myTwoDimensionalArray2[1] = new int[3];
        myTwoDimensionalArray2[1][0] = 4;
        myTwoDimensionalArray2[1][1] = 5;
        myTwoDimensionalArray2[1][2] = 6;
        myTwoDimensionalArray2[2] = new int[4];
        myTwoDimensionalArray2[2][0] = 7;
        myTwoDimensionalArray2[2][1] = 8;
        myTwoDimensionalArray2[2][2] = 9;
        myTwoDimensionalArray2[2][3] = 0;

        // Ülesanne 10
        // Variant 1
        String[][] countryCities = new String[3][];
        countryCities[0] = new String[]{"Tallinn", "Tartu", "Valga", "Võru"};
        countryCities[1] = new String[]{"Stockholm", "Uppsala", "Lund", "Köping"};
        countryCities[2] = new String[]{"Helsinki", "Espoo", "Hanko", "Jämsä"};

        // Variant 2
        String[][] countryCities2 = {
                {"Tallinn", "Tartu", "Valga", "Võru"},
                {"Stockholm", "Uppsala", "Lund", "Köping"},
                {"Helsinki", "Espoo", "Hanko", "Jämsä"}
        };

        // Ülesanne 11
        int numberToPrint = 1;
        while (numberToPrint <= 100) {
            System.out.println(numberToPrint++);
        }

        // Ülesanne 12
        for (int i = 1; i <= 100; i++) {
            System.out.println(i);
        }

        // Ülesanne 13
        // Variant 1
        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        for (int num : numbers) {
            System.out.println(num);
        }

        // Variant 2
        for (int num : new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}) {
            System.out.println(num);
        }

        // Ülesanne 14
        // Variant 1
        for (int i = 1; (i * 3) <= 100; i++) {
            System.out.println(i * 3);
        }

        // Variant 2
        for (int i = 1; i <= 100; i++) {
            if (i % 3 == 0) {
                System.out.println(i);
            }
        }

        // Ülesanne 15
        String[] bands = {"Sun", "Metsatöll", "Queen", "Metallica"};
        String commaSeparatedValues = "";
        for (int i = 0; i < bands.length; i++) {
            commaSeparatedValues = commaSeparatedValues + bands[i];
            if (i + 1 < bands.length) {
                commaSeparatedValues = commaSeparatedValues + ", ";
            }
        }
        System.out.println(commaSeparatedValues);

        // Ülesanne 16
        commaSeparatedValues = "";
        for (int i = bands.length - 1; i >= 0; i--) {
            commaSeparatedValues = commaSeparatedValues + bands[i];
//            commaSeparatedValues = (i > 0) ? commaSeparatedValues + ", " : commaSeparatedValues + "";
            if (i > 0) {
                commaSeparatedValues = commaSeparatedValues + ", ";
            }
        }
        System.out.println(commaSeparatedValues);

        // Ülesanne 17
        String[] numberWords = {
                "null", "üks", "kaks", "kolm", "neli", "viis",
                "kuus", "seitse", "kaheksa", "üheksa"
        };

        if (args.length > 0) {
            int index = 0;
            for (String numberText : args) {
                int numberValue = Integer.parseInt(numberText);
                System.out.print(numberWords[numberValue]);
                if (index < args.length - 1) {
                    System.out.print(", ");
                }
                index++;
            }
        }

//        // Scanneriga lahendus
//        Scanner digitScanner = new Scanner(System.in); // Loome Scanner objekti
//        int[] digitsToPrint = new int[10_000]; //
//        int ii = 0;
//        for(; ii < digitsToPrint.length; ii++) {
//            System.out.println("Sisesta number vahemikus 0 - 9:");
//            String insertedValue = digitScanner.nextLine();
//            insertedValue = insertedValue.trim();
//            if (insertedValue.length() == 0) {
//                break;
//            } else {
//                digitsToPrint[ii] = Integer.parseInt(insertedValue);
//            }
//            if (ii == digitsToPrint.length - 1) {
//                System.out.println("Sorry, rohkem ei saa, käivita palun programm uuesti, kui soovid sisestada veel numbreid!");
//            }
//        }
//
//        // Selleks hetkeks on meil vajalikud numbrid olemas!
//        for (int i = 0; i < ii; i++) {
//            int digitToPrint = digitsToPrint[i]; // Numbriline väärtus
//            System.out.print(numberWords[digitToPrint]);
//            if (i < ii) {
//                System.out.print(", ");
//            }
//        }

//        // Igav lahendus
//        Scanner numberReaderScanner = new Scanner(System.in);
//        while (true) {
//            System.out.println("Sisesta number vahemikus 0 - 9:");
//            String insertedNumber = numberReaderScanner.nextLine();
//            if (insertedNumber.length() == 0) { // Kas soovime programmi tööd lõpetada?
//                break;
//            } else {
//                int insertedNumberDigit = Integer.parseInt(insertedNumber);
//                System.out.println(numberWords[insertedNumberDigit]);
//            }
//        }

//        Scanner numberReaderScanner = new Scanner(System.in);
//        while (true) {
//            System.out.println("Sisesta number vahemikus 0 - 9:");
//            int insertedNumber = numberReaderScanner.nextInt();
//            if (insertedNumber < 0) {
//                break;
//            }
//            switch (insertedNumber) {
//                case 0:
//                    System.out.println("null");
//                    break;
//                case 1:
//                    System.out.println("üks");
//                    break;
//                case 2:
//                    System.out.println("kaks");
//                    break;
//                default:
//                    System.out.println("VIGA: ebakorrektne number");
//            }
//        }

        // Ülesanne 18
        System.out.println();
        double randomNumber;
        do {
            System.out.println("tere");
            randomNumber = Math.random();
        } while (randomNumber < 0.5);

    }
}
