package week1;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class StringProcessing2Ex {

    public static void main(String[] args) {

        // Ülesanne 1
        String president1 = "Konstantin Päts";
        String president2 = "Lennart Meri";
        String president3 = "Arnold Rüütel";
        String president4 = "Toomas Hendrik Ilves";
        String president5 = "Kersti Kaljulaid";
        StringBuilder sentenceBuilder = new StringBuilder();
        sentenceBuilder.append(president1);
        sentenceBuilder.append(", ");
        sentenceBuilder.append(president2);
        sentenceBuilder.append(", ");
        sentenceBuilder.append(president3);
        sentenceBuilder.append(", ");
        sentenceBuilder.append(president4);
        sentenceBuilder.append(" ja ");
        sentenceBuilder.append(president5);
        sentenceBuilder.append(" on Eesti presidendid.");
        System.out.println(sentenceBuilder);

        // Ülesanne 2
        // Variant 1:
        Scanner textProcessor = new Scanner("Rida: See on esimene rida. Rida: See on teine rida. Rida: See on kolmas rida.");
        textProcessor.useDelimiter("Rida: ");
        while (textProcessor.hasNext()) {
            System.out.println(textProcessor.next());
        }

        // Variant 2:
        String text = "Rida: See on esimene rida. Rida: See on teine rida. Rida: See on kolmas rida.";
        String[] rows = text.split("Rida: ");

        for (String row : rows) {
            System.out.println(row);
        }

        for (int i = 0; i < rows.length; i++) {
            System.out.println(rows[i]);
        }

//        List<Integer> list = Arrays.asList(3, 5, 7);


    }
}
